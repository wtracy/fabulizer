// Copyright (c) 2020 William Tracy

varying highp vec2 vTextureCoord;

uniform sampler2D uSampler;
uniform mediump int uHue;
uniform mediump int uLuminance;
uniform mediump int uChroma;

// Convert from sRGB space to linear space.
mediump float gammaExpand(mediump float u) {
    if (u <= 0.04045)
        return u / 12.92;
    return pow((u+0.055)/1.055, 2.4);
}

// Convert from linear space to sRGB space.
mediump float gammaCompress(mediump float u) {
    if (u <= 0.0031308)
        return 12.92 * u;
    return 1.055*pow(u, 0.4166666666666667) - 0.055;
}

mediump float f(mediump float t) {
    if (t > 0.008856)
        return pow(t, 1.0/3.0);
    return 7.787*t + 16.0/116.0;
}

mediump float fInverse(mediump float t) {
    mediump float delta = 0.20689655172413793;
    if (t > delta)
        return pow(t, 3.0);
    return 3.0 * pow(delta, 2.0) * (t-0.13793103448275862);
}

const mediump mat3 RGB_TO_XYZ = mat3(
        0.4124564, 0.2126729, 0.0193339,
        0.3575761, 0.7151522, 0.1191920,
        0.1804375, 0.0721750, 0.9503041
);

const mediump mat3 XYZ_TO_RGB = mat3(
        3.2404542,-0.9692660, 0.0556434,
        -1.5371385, 1.8760108,-0.2040259,
        -0.4985314, 0.0415560, 1.0572252
);

void main(void) {
    mediump vec4 raw = texture2D(uSampler, vTextureCoord);
    mediump vec3 rgb;
    mediump vec3 xyz;

    rgb.x = gammaExpand(raw.r);
    rgb.y = gammaExpand(raw.g);
    rgb.z = gammaExpand(raw.b);

    // Convert to CIE XYZ
    xyz = RGB_TO_XYZ * (rgb * 100.0);

    // Convert to L*a*b*
    mediump float l;
    mediump float a;
    mediump float b;

    l = 116.0 * f(xyz.y/100.0) - 16.0;
    a = 500.0 * (f(xyz.x/95.0489) - f(xyz.y/100.0));
    b = 200.0 * (f(xyz.y/100.0) - f(xyz.z/108.8840));

    // Convert to LCH(ab)
    mediump float c;
    mediump float h;

    c = sqrt(a*a + b*b);
    // Set everything to the same hue
    h = radians(float(uHue));

    // Average out the chroma
    c = float(uChroma)*0.4 + c*0.6;
    // Average out the luminance
    l = float(uLuminance)*0.5 + l*0.5;

    // Convert back to L*a*b*
    a = c * cos(h);
    b = c * sin(h);

    // Convert back to XYZ
    xyz.x = 95.0489  * fInverse((l+16.0)/116.0 + a/500.0);
    xyz.y = 100.0    * fInverse((l+16.0)/116.0);
    xyz.z = 108.8840 * fInverse((l+16.0)/116.0-b/200.0);

    //Convert back to linear RGB
    rgb = XYZ_TO_RGB * (xyz*0.01);

    gl_FragColor.r = gammaCompress(rgb.r);
    gl_FragColor.g = gammaCompress(rgb.g);
    gl_FragColor.b = gammaCompress(rgb.b);
    gl_FragColor.a = 1.0;
}
