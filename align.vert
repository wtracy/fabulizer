// Copyright (c) 2020 William Tracy

attribute vec4 aVertexPosition;
attribute vec2 aTextureCoord;

varying highp vec2 vTextureCoord;

void main() {
        gl_Position = mat4(
            2.0, 0.0, 0.0, 0.0,
            0.0, -2.0, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            -1.0, 1.0, 0.0, 1.0
        ) * aVertexPosition;
        vTextureCoord = aTextureCoord;
}
