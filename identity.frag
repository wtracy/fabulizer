// Copyright (c) 2020 William Tracy

varying highp vec2 vTextureCoord;

uniform sampler2D uSampler;

void main(void) {
    gl_FragColor = texture2D(uSampler, vTextureCoord);
    //gl_FragColor.g = 0.0;
}
