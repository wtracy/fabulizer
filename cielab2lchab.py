from math import atan2, degrees, sqrt
from sys import argv

# Accepts three floating-point arguments, representing the L*, a*, and b* input
# values. L* is optional.
#
# Outputs LCH(ab) chroma and hue values, with chroma marked c, and hue
# marked h.

def main():
    a = float(argv[-2])
    b = float(argv[-1])

    c = sqrt(a*a + b*b)
    h = atan2(b, a)
    if h < 0:
        h = h + 6.283185307179586

    print('c = ' + str(c))
    print('h = ' + str(degrees(h)))

if __name__ == '__main__':
    main()
