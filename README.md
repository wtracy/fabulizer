# Introduction

Fabulizer is a web app that applies various Pride-themed filters to user-supplied images.

Use it live at http://fabulize.me/

# Licensing

The peacock image is used here under the terms of the Creative Commons
Attribution-ShareAlike 4.0 International license. It is owned by the Wikimedia
Commons user Acabashi. The original may be downloaded at 
https://commons.wikimedia.org/wiki/File:%27Pavo%27_peacock_at_Blake_End,_Great_Saling,_Essex,_England_02.jpg

All other content is copyright 2020 by William Tracy, and is made available under the terms of the MIT software license.

# Development

## Setup

Due to [CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS) limitations, you need an HTTP server to test Fabulizer, even locally. 

If you have Python 3 installed, you can use the built-in web server:

`$ python3 -m http.server 8000`

If you have NodeJS installed, you can use its built-in web server:

`$ http-server`

## Project Structure

All of the project's Javascript code is inline in `index.html`. Image filters
are defined in `filters.json`. WebGL vertex and fragment shaders live in
`.vert` and `.frag` files respectively. There are two Python scripts for
converting sRGB and CIE L\*a\*b\* colors to LCH(ab) format, the format required
for defining filters. See the opening comment within each script for details.

## Writing New Filters

`filters.json` contains an array of filter objects. Each filter object has two parameters: `name`, which must be unique to that filter, and `pattern`, which contains an array of shape objects.

### Specifying Shaders and Shader Parameters

Each shape object has a `shader` parameter. This should be the name of a fragment shader file. As of this writing, the valid shaders are `lchab.frag`, `gray.frag`, and `identity.frag`.

`gray.frag` expects the shape to have a `luminance` parameter with a value between 0 (black) and 100 (white).

`lchab.frag` expects the shape object to have `luminance` specified, as well as a `hue` value between 0 and 360 (a location on the color wheel) and a `chroma` between 0 (gray) and 100 (vivid color). The included Python scripts can help in calculating these values from sRGB or CIE L\*a\*b\* tuples. (See the opening comment within each script for details.)

`identity.frag` expects no other parameters and does not modify the input image.

### Shape Geometry

Regardless of the shader you choose, you need to specify a `geometry` for the shape. This is specified as an array of coordinate values. (If you are familiar with OpenGL, this is drawn as a `GL_TRIANGLE_STRIP`.)

The first two numbers specify the first point, the next two numbers specify the second point, and so on. <0, 0> is the upper-left corner of the image, and <1, 1> is the bottom-right corner of the image. 

The first three points specify the first triangle, and then the next point and the most recently defined edge define the second triangle. Each triangle must share an edge with the triangle before it (if any) and an edge with the triangle after it (if any). The easy way to manage this when drawing rectangles is to first define the upper edge from left to right, and then define the bottom edge from left to right. The important thing is for the "middle" two vertices to share a diagonal.

As of this writing, each geometry definition must exactly four vertices, meaning that you can only define quadrilaterals. This should be resolved in a future version of Fabulizer.

You can test changes to your filters without refreshing the entire page by pressing the `r` key to reload just the filters.

## Future Work

* Shape geometry should not be fixed to four vertices.
* Add circle support for the [intersex flag](https://morgancarpenter.com/intersex-flag/) and the [safe space/straight ally symbol](https://commons.wikimedia.org/wiki/File:Straightally.svg).
* Add support for shapes scaled to fit inside the image, rather than stretched to fit the image. The shape could exist inside a square (with <0, 0> being the upper left corner, and <1, 1> being the lower right corner) and the square would be scaled and centered within the image.
* User-controlled chroma and luminocity weights would be fun. (Create sliders that allow the end user to adjust the weights between the source image and the filter pattern.)
* Add integration with Facebook/Instagram/whatever the cool kids are using these days. This would look like a set of easy-to-use "share" buttons.

# About Me

My name is William Tracy, and I'm a software engineer based in Santa Rosa, California. Most of my experience is with backend and semi-embedded systems, though you can see here that I'm learning my way around frontend development. I'm currently looking for short-term or long-term remote work opportunities. My contact information is [in my resume](http://wtracy.com/Resume.pdf).
