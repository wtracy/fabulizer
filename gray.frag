// Copyright (c) 2020 William Tracy

varying highp vec2 vTextureCoord;

uniform sampler2D uSampler;
uniform mediump int uLuminance;

// Convert from sRGB space to linear space.
mediump float gammaExpand(mediump float u) {
    if (u <= 0.04045)
        return u / 12.92;
    return pow((u+0.055)/1.055, 2.4);
}

// Convert from linear space to sRGB space.
mediump float gammaCompress(mediump float u) {
    if (u <= 0.0031308)
        return 12.92 * u;
    return 1.055*pow(u, 0.4166666666666667) - 0.055;
}

mediump float f(mediump float t) {
    if (t > 0.008856)
        return pow(t, 1.0/3.0);
    return 7.787*t + 16.0/116.0;
}

mediump float fInverse(mediump float t) {
    mediump float delta = 0.20689655172413793;
    if (t > delta)
        return pow(t, 3.0);
    return 3.0 * pow(delta, 2.0) * (t-0.13793103448275862);
}



const mediump mat3 RGB_TO_XYZ = mat3(
        0.4124564, 0.2126729, 0.0193339,
        0.3575761, 0.7151522, 0.1191920,
        0.1804375, 0.0721750, 0.9503041
);

const mediump mat3 XYZ_TO_RGB = mat3(
        3.2404542,-0.9692660, 0.0556434,
        -1.5371385, 1.8760108,-0.2040259,
        -0.4985314, 0.0415560, 1.0572252
);

void main(void) {
    mediump vec4 raw = texture2D(uSampler, vTextureCoord);
    mediump vec3 v;

    v.x = gammaExpand(raw.r);
    v.y = gammaExpand(raw.g);
    v.z = gammaExpand(raw.b);

    // Convert to CIE XYZ
    v = RGB_TO_XYZ * (v * 100.0);

    // Extract L*a*b* luminance
    mediump float luminance = 116.0 * f(v.y/100.0) - 16.0;
    luminance = luminance*0.4 + float(uLuminance)*0.6;

    v.x = v.y = v.z = luminance * 0.01;
    gl_FragColor.rgb = v;
    gl_FragColor.a = 1.0;
}
