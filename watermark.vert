// Copyright (c) 2020 William Tracy

attribute vec4 aVertexPosition;
attribute vec2 aTextureCoord;
uniform float uWidthFactor;
uniform float uHeightFactor;

varying highp vec2 vTextureCoord;

void main() {
        gl_Position = mat4(
            1.0, 0.0, 0.0, 0.0,
            0.0, -1.0, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            -1.0, 1.0, 0.0, 1.0
        ) * aVertexPosition;
        gl_Position = mat4(
            uWidthFactor, 0.0, 0.0, 0.0,
            0.0, uHeightFactor, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            1.0, -1.0, 0.0, 1.0
        ) * gl_Position;
        vTextureCoord = aTextureCoord;
}
