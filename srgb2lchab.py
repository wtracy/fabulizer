from math import atan2, degrees, sqrt
from sys import argv

from numpy import dot, fromiter, multiply

delta = 0.20689655172413793

# Converts (decimal) sRGB values to LCH(ab) values. It expects three
# integers as command line parameters, each in the range from 0 to 255.
#
# Outputs intermediate CIE XYZ and CIE L*a*b* values for debugging purposes.
#
# The last five lines of output are the three L*a*b* values, leading with
# luminance, followed the chroma and hue values for LCH(ab). Chroma is
# marked c, and hue is marked h.
#
# This program uses Numpy for developer ergonomics. (Numpy certainly doesn't
# make it execute any faster!) It requires a Numpy installation to run.

def gamma_expand(u):
    if u <= 0.04045:
        return u / 12.92
    return pow((u+0.055)/1.055, 2.4)

def gamma_compress(u):
    if (u <= 0.0031308):
        return 12.92 * u
    return 1.055*pow(u, 0.4166666666666667) - 0.055

def f(t):
    if (t > 0.008856):
        return pow(t, 1/3)
    return 7.787*t + 16/116

def fInverse(t):
    if (t > delta):
        return pow(t, 3.0)
    return 3.0 * pow(delta, 2.0) * (t-0.13793103448275862)

RGB_TO_XYZ = [
    [0.41239080, 0.35758434, 0.18048079],
    [0.21263901, 0.71516868, 0.07219232],
    [0.01933082, 0.11919478, 0.95053215]
]

XYZ_TO_RGB = [
    [3.24096994, -1.53738318, -0.49861076],
    [-0.96924364, 1.8759675, 0.04155506],
    [0.05563008, -0.20397696, 1.05697151]
]

def main():
    rgb = parse_args()
    rgb = rgb / 255

    for i in range(3):
        rgb[i] = gamma_expand(rgb[i])

    xyz = dot(RGB_TO_XYZ, rgb * 100)

    print ("x = " + str(xyz[0]))
    print ("y = " + str(xyz[1]))
    print ("z = " + str(xyz[2]))

    l = 116.0 * f(xyz[1]/100.0) - 16.0
    a = 500.0 * (f(xyz[0]/95.0489) - f(xyz[1]/100.0))
    b = 200.0 * (f(xyz[1]/100.0) - f(xyz[2]/108.8840))

    print("l = " + str(l))
    print("a = " + str(a))
    print("b = " + str(b))

    c = sqrt(a*a + b * b)
    h = atan2(b, a)
    if h < 0:
        h = h + 6.283185307179586

    print("c = " + str(c))
    print("h = " + str(degrees(h)))


def parse_args():
    integers = [argv[-3], argv[-2], argv[-1]]
    return fromiter(map(int, integers), int)

if __name__ == '__main__':
    main()
